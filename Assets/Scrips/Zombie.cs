using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private float Vida = 5;
    private bool bala_p = false;
    private bool bala_m = false;
    private bool bala_g = false;
    
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
       
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("vida: " + Vida);
        if (bala_p)
        {
            if (Vida == 0)
                Destroy(this.gameObject);
            bala_p = false;
        }
        else if (bala_m) 
        {
            if (Vida == 0)
                Destroy(this.gameObject);
            bala_m = false;
        }
        else if(bala_g)
        {
            if (Vida == 0)
                Destroy(this.gameObject);
                bala_g = false;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision2D)
    {
        
        if (collision2D.gameObject.layer == 6)
        {
            Vida--;
            bala_p = true;
        }
        if (collision2D.gameObject.layer == 7)
        {
            Vida = Vida - 2.5f;
            bala_m = true;
        }
        if (collision2D.gameObject.layer == 8)
        {

            Vida = Vida - 5;
            bala_g = true;
        }
    }
}
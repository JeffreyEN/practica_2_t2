using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala_G : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private SpriteRenderer sr;
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        Destroy(this.gameObject, 5);
    }
    void Update()
    {
        if (sr.flipX)
            rb2d.velocity = new Vector2(-5f, rb2d.velocity.y);
        else
            rb2d.velocity = new Vector2(5f, rb2d.velocity.y);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 3)
        {
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
    }

}

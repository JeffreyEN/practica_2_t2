using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject Bala_p_i;
    public GameObject Bala_m_i;
    public GameObject Bala_g_i;

    public GameObject Bala_p_d;
    public GameObject Bala_m_d;
    public GameObject Bala_g_d;

    private const float Bala_M_T = 3;
    private const float Bala_G_T = 5;
    private float time = 1;
    private float switchColorTime = 0;
    private float switchColorDelay = 0.1f;

    private bool EstaTocandoElSuelo = false;
    public int upSpeed = 60;

    private Rigidbody2D rb2d;
    public SpriteRenderer sr;
    private Transform _transform;
    private Animator _animator;
    private Color originalColor;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        _transform = GetComponent<Transform>();
        _animator = GetComponent<Animator>();
        originalColor = sr.color;
    }

    void Update()
    {
        

        if (Input.GetKey(KeyCode.RightArrow))
        {
            sr.flipX = false;
            setCorrerAnimation();
            rb2d.velocity = new Vector2(30, rb2d.velocity.y);
            
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            sr.flipX = true;
            setCorrerAnimation();
            rb2d.velocity = new Vector2(-30, rb2d.velocity.y);
            
        }
        else if (Input.GetKeyDown(KeyCode.Space) && EstaTocandoElSuelo)
        {
            setSaltarAnimation();
            rb2d.velocity = Vector2.up * upSpeed;
            EstaTocandoElSuelo = false;
        }
        else
        {
            setIdleAnimation();
            rb2d.velocity = new Vector2(0, rb2d.velocity.y);
        }
        if (Input.GetKey("x"))
        {
            Parpadear();
            time += Time.deltaTime;
            Debug.Log("Tiempo: " + time);
        }
        if (Input.GetKeyUp("x"))
        {
            if (time <Bala_M_T)
            {
                if (sr.flipX)
                {
                    setCorrer_DisparandoAnimation();
                    var BulletPosition = new Vector3(_transform.position.x - 1f, _transform.position.y, _transform.position.z);
                    Instantiate(Bala_p_i, BulletPosition, Quaternion.identity);

                }
                if (!sr.flipX)
                {
                    setCorrer_DisparandoAnimation();
                    var BulletPosition = new Vector3(_transform.position.x + 1f, _transform.position.y, _transform.position.z);
                    Instantiate(Bala_p_d, BulletPosition, Quaternion.identity);
                }
                
            }
            else if (time < Bala_G_T)
            {
                if (sr.flipX)
                {
                    setCorrer_DisparandoAnimation();
                    var BulletPosition = new Vector3(_transform.position.x - 1f, _transform.position.y, _transform.position.z);
                    Instantiate(Bala_m_i, BulletPosition, Quaternion.identity);
                }
                if (!sr.flipX)
                {
                    setCorrer_DisparandoAnimation();
                    var BulletPosition = new Vector3(_transform.position.x + 1f, _transform.position.y, _transform.position.z);
                    Instantiate(Bala_m_d, BulletPosition, Quaternion.identity);
                }
                
            }
            else if (time >= Bala_G_T)
            {
                if (sr.flipX)
                {
                    setCorrer_DisparandoAnimation();
                    var BulletPosition = new Vector3(_transform.position.x - 1f, _transform.position.y, _transform.position.z);
                    Instantiate(Bala_g_i, BulletPosition, Quaternion.identity);
                }
                if (!sr.flipX)
                {
                    setCorrer_DisparandoAnimation();
                    var BulletPosition = new Vector3(_transform.position.x + 1f, _transform.position.y, _transform.position.z);
                    Instantiate(Bala_g_d, BulletPosition, Quaternion.identity);
                }
                
            }
                time = 1;
            sr.color = originalColor;
        }

    }
    private void Parpadear()
    {
        switchColorTime += Time.deltaTime;
        if (switchColorTime > switchColorDelay)
        {
            if (sr.color == originalColor)
                sr.color = Color.blue;
            else
                sr.color = originalColor;
            switchColorTime = 0;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        EstaTocandoElSuelo = true;
    }
    private void setIdleAnimation()
    {
        _animator.SetInteger("Estado", 0);
    }
    private void setCorrerAnimation()
    {
        _animator.SetInteger("Estado", 1);
    }
    private void setSaltarAnimation()
    {
        _animator.SetInteger("Estado", 2);
    }
    private void setCorrer_DisparandoAnimation()
    {
        _animator.SetInteger("Estado", 3);
    }
}
